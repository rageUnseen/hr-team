package com.recruitementportal.contoller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.recruitementportal.model.Applicant;
import com.recruitementportal.model.ApplicationStatus;
import com.recruitementportal.model.Job;
import com.recruitementportal.model.Skill;
import com.recruitementportal.service.HrService;

@Controller
public class HrController {

	@Autowired
	HrService hrService;
	
	private List<Applicant> getDummyList(){
		List<Applicant> applicationList = new ArrayList<>();
		Applicant applicant = new Applicant();
		ApplicationStatus status = new ApplicationStatus();
		status.setResumeShortList(false);
		applicant.setApplicationStatus(status);
		applicant.setApplicantID(1234);
		Skill skill1 = new Skill();
		Skill skill2 = new Skill();
		skill1.setSkillType("java");
		skill2.setSkillType("python");
		Set<Skill> skillSet = new HashSet<>();
		skillSet.add(skill1);
		skillSet.add(skill2);
		applicant.setSkillset(skillSet);
		applicant.setName("test");
		applicant.setEmail("fdjsbfs");
		applicant.setQualifications("expert");
		applicant.setType("gradutaed");
		applicant.setExperience(5);
		applicationList.add(applicant);
		return applicationList;
	}
	
	@RequestMapping(value="/takeToHome")
	public String takeToHome() {
		return "Home";
	}
	
	@RequestMapping(value="/jobPost")
	public String jobPosting() {
		return "jobForm";
	}
	
	
	@PostMapping(value="/gotoJobPost")
	public String sendJobData(@ModelAttribute Job job,Model model) {
		String[] skillsArray = job.getSkills().split(",");
		Set<Skill> skillSet=hrService.createJobObjectToSend(skillsArray);
		job.setSkillSet(skillSet);
		MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
		
		paramMap.add("title", job.getTitle());
		paramMap.add("dept", job.getDept());
		paramMap.add("location", job.getLocation());
		paramMap.add("profile", job.getProfile());
		paramMap.add("experience", job.getExperience());
		paramMap.add("type", job.getType());
		paramMap.add("vacancy", job.getVacancy());
		paramMap.add("skillSet", job.getSkillSet());

		RestTemplate restTemplate = new RestTemplate();
		String url="";//need this url from job team
		//restTemplate posts the requirement
		//this method is complete
		restTemplate.postForObject(url, paramMap, String.class);
		return "Home";
	}
	
	@PostMapping(value="/reviewResumeUpdate")
	public ModelAndView reviewResumeUpdate(@RequestParam("reviewResume")String reviewResume,
			@RequestParam("applicantId") Integer applicantId
			,Model model) {
		hrService.updateResumeStatus(reviewResume,applicantId);
		return new ModelAndView("redirect:/reviewApplications");
	}
	@PostMapping(value="/reviewTechUpdate")
	public ModelAndView reviewTechUpdate(@RequestParam("reviewTech")String reviewTech,
			@RequestParam("applicantId") Integer applicantId
			,Model model) {
		hrService.updateTechStatus(reviewTech,applicantId);
		return new ModelAndView("redirect:/getResumeApplications");
	}
	@PostMapping(value="/reviewHrUpdate")
	public ModelAndView reviewHrUpdate(@RequestParam("reviewHr")String reviewHr,
			@RequestParam("applicantId") Integer applicantId
			,Model model) {
		hrService.updateHrStatus(reviewHr,applicantId);
		return new ModelAndView("redirect:/getTechApplications");
	}
	@PostMapping(value="/reviewAppointUpdate")
	public ModelAndView reviewAppointUpdate(@RequestParam("reviewAppoint")String reviewAppoint,
			@RequestParam("applicantId") Integer applicantId
			,Model model) {
		hrService.updateAppointedStatus(reviewAppoint,applicantId);
		return new ModelAndView("redirect:/getHrApplications");
	}
	
	@RequestMapping(value="/getApplications")
	public String getApplicantsData(Model model) {
		//put the entire list on applicationData jsp
		//list name in jsp is applicationList
		List<Applicant> applicationList = getDummyList();
		model.addAttribute("applicationList", applicationList);
		return "applicationData";
		
		
	}
	
	@RequestMapping(value="/getResumeApplications")
	public String getResumeData(Model model) {
		//put the entire list on applicationData jsp
		//list name in jsp is applicationList
		
		List<Applicant> applList = getDummyList().
		stream().filter((x)->x.getApplicationStatus().isResumeShortList()==true 
		&& x.getApplicationStatus().isTechInterview()==false)
		.collect(Collectors.toList());
		model.addAttribute("applicationList", applList);
		return "reviewTechInterview";
		
		
	}
	
	@RequestMapping(value="/getTechApplications")
	public String getTechData(Model model) {
		//put the entire list on applicationData jsp
		//list name in jsp is applicationList
		
		List<Applicant> applList = getDummyList().
		stream().filter((x)->x.getApplicationStatus().isTechInterview()==true
		&& x.getApplicationStatus().isHrInterview()==false)
		.collect(Collectors.toList());
		model.addAttribute("applicationList", applList);
		return "reviewHrInterview";
		
		
	}
	
	@RequestMapping(value="/getHrApplications")
	public String getHrData(Model model) {
		//put the entire list on applicationData jsp
		//list name in jsp is applicationList
		
		List<Applicant> applList = getDummyList().
		stream().filter((x)->x.getApplicationStatus().isHrInterview()==true 
		&& x.getApplicationStatus().isAppointed()==false)
		.collect(Collectors.toList());
		model.addAttribute("applicationList", applList);
		return "reviewAppointed";
		
		
	}
	
	@RequestMapping(value="/reviewApplications")
	public String reviewApplicantsData(Model model) {
		//put the entire list on reviewResume jsp with (true or false field) 
		//put the next button which is a submit button and go to reviewTech jsp
		List<Applicant> applList = getDummyList().
				stream().filter((x)->x.getApplicationStatus().isResumeShortList()==false)
				.collect(Collectors.toList());
		model.addAttribute("applicationList", applList);
		return "reviewResume";
	}
}

package com.recruitementportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRecruitmentportalHrWarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRecruitmentportalHrWarApplication.class, args);
	}

}

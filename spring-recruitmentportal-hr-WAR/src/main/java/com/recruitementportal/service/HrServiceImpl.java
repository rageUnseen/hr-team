package com.recruitementportal.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.recruitementportal.model.Applicant;
import com.recruitementportal.model.ApplicationStatus;
import com.recruitementportal.model.Skill;

@Service
public class HrServiceImpl implements HrService{

	@Autowired
	Applicant applicant;
	@Autowired
	ApplicationStatus applicationStatus;
	@Override
	public void updateResumeStatus(String reviewResume, Integer applicantId) {
		// TODO Auto-generated method stub
		applicationStatus.setApplicationId(applicantId);
		if(reviewResume=="YES")
			applicationStatus.setResumeShortList(true);
		else
			applicationStatus.setResumeShortList(false);
	}
	@Override
	public void updateTechStatus(String reviewTech, Integer applicantId) {
		// TODO Auto-generated method stub
		applicationStatus.setApplicationId(applicantId);
		if(reviewTech=="YES")
			applicationStatus.setTechInterview(true);
		else
			applicationStatus.setTechInterview(false);
			
	}
	@Override
	public void updateHrStatus(String reviewHr, Integer applicantId) {
		// TODO Auto-generated method stub
		applicationStatus.setApplicationId(applicantId);
		if(reviewHr=="YES")
			applicationStatus.setHrInterview(true);
		else
			applicationStatus.setHrInterview(false);

	}
	@Override
	public void updateAppointedStatus(String reviewAppoint, Integer applicantId) {
		// TODO Auto-generated method stub
		applicationStatus.setApplicationId(applicantId);
		if(reviewAppoint=="YES")
			applicationStatus.setAppointed(true);
		else
			applicationStatus.setAppointed(false);
	}
	@Override
	public Set<Skill> createJobObjectToSend(String[] skillsArray) {
		// TODO Auto-generated method stub
		Set<Skill> skillSet = new HashSet<>();
		for(int i=0;i<skillsArray.length;i++)
		{
			Skill skill = new Skill();
			skill.setSkillType(skillsArray[i]);
			skillSet.add(skill);
		}
		return skillSet;
	}
	
	
	

}

package com.recruitementportal.service;

import java.util.Set;

import com.recruitementportal.model.Skill;

public interface HrService {


	void updateResumeStatus(String reviewResume, Integer applicantId);
	void updateTechStatus(String reviewTech, Integer applicantId);
	void updateHrStatus(String reviewHr, Integer applicantId);
	void updateAppointedStatus(String reviewAppoint, Integer applicantId);
	Set<Skill> createJobObjectToSend(String[] skillsArray);

	
}

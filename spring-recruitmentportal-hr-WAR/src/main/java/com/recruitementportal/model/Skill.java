package com.recruitementportal.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

//@Entity
public class Skill {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "skillseq")
	@SequenceGenerator(name = "skillseq", sequenceName = "SKILL_SEQ")
	@Column(name = "skillid")
	private Integer skillId;
	@Column(name = "skilltype")
	private String skillType;
	private Set<Applicant> applicantList;

	public Integer getSkillId() {
		return skillId;
	}

	public void setSkillId(Integer skillId) {
		this.skillId = skillId;
	}

	public String getSkillType() {
		return skillType;
	}

	public void setSkillType(String skillType) {
		this.skillType = skillType;
	}

	public Set<Applicant> getApplicantList() {
		return applicantList;
	}

	public void setApplicantList(Set<Applicant> applicantList) {
		this.applicantList = applicantList;
	}

	@Override
	public String toString() {
		return "Skill [skillId=" + skillId + ", skillType=" + skillType + ", applicantList=" + applicantList + "]";
	}

}
package com.recruitementportal.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.springframework.stereotype.Component;
//@Entity
@Component
public class Applicant {
	private String name;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "applicantseq")
	@SequenceGenerator(name="applicantseq", sequenceName = "APPLICANT_SEQ")
	private Integer applicantID;
	private String email;
	private String password;
	private Set<Skill> skillSet;
	private ApplicationStatus applicationStatus;
	private Date dob;
	private String qualifications;
	//ft, pt or intern
	private String type;
	//private String applicationStatus;
	private int experience;
	
	/*public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}*/
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getApplicantID() {
		return applicantID;
	}
	public Set<Skill> getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(Set<Skill> skillSet) {
		this.skillSet = skillSet;
	}
	public ApplicationStatus getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(ApplicationStatus applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public void setApplicantID(Integer applicantID) {
		this.applicantID = applicantID;
	}
	public void setApplicantID(int applicantID) {
		this.applicantID = applicantID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<Skill> getSkillset() {
		return skillSet;
	}
	public void setSkillset(Set<Skill> skillset) {
		this.skillSet = skillset;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getQualifications() {
		return qualifications;
	}
	public void setQualifications(String qualifications) {
		this.qualifications = qualifications;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	@Override
	public String toString() {
		return "Applicant [name=" + name + ", applicantID=" + applicantID + ", email=" + email + ", password="
				+ password + ", skillSet=" + skillSet + ", dob=" + dob + ", qualifications=" + qualifications
				+ ", type=" + type + ", applicationStatus=" + applicationStatus + ", experience=" + experience + "]";
	}
	
}

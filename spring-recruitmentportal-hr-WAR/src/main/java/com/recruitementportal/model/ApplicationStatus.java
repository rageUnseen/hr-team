package com.recruitementportal.model;

import org.springframework.stereotype.Component;

@Component
public class ApplicationStatus {

	private Integer applicationId;
	private Integer jobId;
	private boolean resumeShortList;
	private boolean techInterview;
	private boolean hrInterview;
	private boolean appointed;
	private Applicant applicant;
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getJobId() {
		return jobId;
	}
	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}
	public boolean isResumeShortList() {
		return resumeShortList;
	}
	public void setResumeShortList(boolean resumeShortList) {
		this.resumeShortList = resumeShortList;
	}
	public boolean isTechInterview() {
		return techInterview;
	}
	public void setTechInterview(boolean techInterview) {
		this.techInterview = techInterview;
	}
	public boolean isHrInterview() {
		return hrInterview;
	}
	public void setHrInterview(boolean hrInterview) {
		this.hrInterview = hrInterview;
	}
	public boolean isAppointed() {
		return appointed;
	}
	public void setAppointed(boolean appointed) {
		this.appointed = appointed;
	}
	public Applicant getApplicant() {
		return applicant;
	}
	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}
	
}


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Applicants List</title>
</head>
<body>

	<table border="1">
		<tr>
			<th>Applicant Id</th>
			<th>Name</th>
			<th>Email</th>
			<th>DOB</th>
			<th>qualifications</th>
			<th>Type</th>
			<th>Experience</th>
		</tr>


		<tr>
			<c:forEach items="${applicationList}" var="applicant">

				<td>${applicant.applicantID}</td>
				<td>${applicant.name}</td>
				<td>${applicant.email}</td>
				<td>${applicant.dob}</td>
				<td>${applicant.qualifications}</td>
				<td>${applicant.type}</td>
				<td>${applicant.experience}</td>

			</c:forEach>
		</tr>
	</table>
	
</body>
</html>